#include "mainwindow.h"

#include <QComboBox>
#include <QLayout>
#include <QPushButton>
#include <QDebug>

#include "Widgets/loggingwidget.h"
#include "Widgets/measurementwidget.h"
#include "Widgets/optionswidget.h"
#include "Widgets/fecc.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      lastAverageCount(0),
      voltageCounter(0),
      voltageSum(0.0),
      voltageValue(0.0)
{
    qRegisterMetaType<BasicDataStruct>("BasicDataStruct");

    openPowerMeter = new OpenPowerMeter(this);
    measurementWidget = new MeasurementWidget(this);
    optionsWidget = new OptionsWidget(this);
    outputLog = new LoggingWidget(this);
    fecc = new FECC(this);
    pwm = new PowerMeter();

    measurementWidget->setEnableMeasurements(false);

    QVBoxLayout *optionsLayout = new QVBoxLayout;
    optionsLayout->addWidget(fecc);
    optionsLayout->addWidget(optionsWidget);

    QVBoxLayout *leftLayout = new QVBoxLayout;
    leftLayout->addWidget(openPowerMeter);
    leftLayout->addWidget(measurementWidget, 1);
    leftLayout->addLayout(optionsLayout);

    QVBoxLayout *rightLayout = new QVBoxLayout;
    rightLayout->addWidget(outputLog);

    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addLayout(leftLayout);
    mainLayout->addLayout(rightLayout);

    QWidget *mainWidget = new QWidget(this);
    mainWidget->setLayout(mainLayout);

    setCentralWidget(mainWidget);

    autoUpdateTimer = new QTimer(this);
    connect(autoUpdateTimer, &QTimer::timeout, this, &MainWindow::readVoltageFromPowermeter);

    connect(openPowerMeter, &OpenPowerMeter::refreshList, this, &MainWindow::refreshDeviceList);
    connect(openPowerMeter, &OpenPowerMeter::connectToPowerMeterSignal, this, &MainWindow::connectSlot);
    connect(openPowerMeter, &OpenPowerMeter::disconnectToPowerMeterSignal, this, &MainWindow::disconnectSlot);

    connect(measurementWidget, &MeasurementWidget::sendAverageCount, this, &MainWindow::setAverageCountSlot);
    connect(measurementWidget, &MeasurementWidget::sendFreq, this, &MainWindow::setMeasureFreqSlot);
    connect(measurementWidget, &MeasurementWidget::sendMeasure, outputLog, &LoggingWidget::addMeasure);

    connect(fecc, &FECC::sendUseFeccTable1, this, &MainWindow::setUseFeccTable1);
    connect(fecc, &FECC::sendUseFeccTable2, this, &MainWindow::setUseFeccTable2);

    connect(optionsWidget, &OptionsWidget::sendPowerUnits, this, &MainWindow::setPowerUnitsTypeSlot);

    refreshDeviceList();
}

MainWindow::~MainWindow()
{
    delete pwm;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {

    case Qt::Key_Escape:
    {
        this->close();
        break;
    }

    }
}

void MainWindow::initVoltageCounter()
{
    voltageCounter = 0;
    voltageSum = 0.0;
}

void MainWindow::startAutoTimer()
{
    initVoltageCounter();

    int timerInterval;
    int averageCount;
    pwm->getAverageCount(averageCount);
    lastAverageCount = averageCount;

    if (averageCount < OUTPUT_INTERVAL / READ_V_INTERVAL)
        timerInterval = (int)roundf((float)OUTPUT_INTERVAL / averageCount);
    else
        timerInterval = READ_V_INTERVAL;

    if (timerInterval <= 0)
        return;

    autoUpdateTimer->start(timerInterval);
}

void MainWindow::stopAutoTimer()
{
    autoUpdateTimer->stop();
}

void MainWindow::readVoltageFromPowermeter()
{
    int averageCount;
    pwm->getAverageCount(averageCount);
    if (averageCount != lastAverageCount)   // Restart timer with new interval
    {
        stopAutoTimer();
        startAutoTimer();
        return;
    }

    double voltage;

    if (pwm->readVoltage(voltage) == PWM_SUCCESS)
    {
        voltageCounter++;
        voltageSum += voltage;

        if (voltageCounter == averageCount)
        {
            voltageValue = voltageSum / voltageCounter;
            initVoltageCounter();

            double power;
            pwm->calculatePower(voltageValue, power);
            pwmPowerUnits powerUnitsFromPWM;
            pwm->getPowerUnits(powerUnitsFromPWM);
            measurementWidget->setPowerValue(voltageValue, power, powerUnitsFromPWM);
        }
    }
    else
    {
        QMessageBox::warning(this, "Error", "Lost connection to powerMeter");
        disconnectSlot();
    }

    lastAverageCount = averageCount;
}

void MainWindow::refreshDeviceList()
{
    int numOfDevices = 0;
    pwm->getNumberOfDevices(numOfDevices);

    std::vector <int> deviceList;
    for (int i = 0; i < numOfDevices; i++)
    {
        int serialNumber = 0;
        pwm->getSerialNumber(i, serialNumber);
        deviceList.push_back(serialNumber);
    }

    openPowerMeter->setDeviceList(deviceList);
}

void MainWindow::connectSlot(int sN)
{
    pwmStatus connectionStatus = pwm->connect(sN);

    if (connectionStatus == PWM_SUCCESS)
    {
        openPowerMeter->onConnected();
        measurementWidget->onConnected();

        measurementWidget->setBasicData(pwm->getBasicData());

        int feccTablesCount;
        pwm->getFECCTablesCount(feccTablesCount);
        fecc->setCountOfFeccTable(feccTablesCount);

        startAutoTimer();
    }
    else if (connectionStatus == PWM_READ_BASIC_ERROR || connectionStatus == PWM_READ_FECC_ERROR || connectionStatus == PWM_READ_PV_ERROR)
    {
        QMessageBox::warning(this, "Error", "Cannot obtain data from powerMeter");
        pwm->disconnect();
        refreshDeviceList();
    }
    else
    {
        QMessageBox::warning(this, "Error", "Failed to connect to powerMeter");
        pwm->disconnect();
        refreshDeviceList();
    }
}

void MainWindow::disconnectSlot()
{
    stopAutoTimer();
    pwm->disconnect();

    openPowerMeter->onDisconnected();
    measurementWidget->onDisconnected();
    fecc->setCountOfFeccTable(1);
}

void MainWindow::setAverageCountSlot(int count)
{
    pwm->setAverageCount(count);
}

void MainWindow::setMeasureFreqSlot(double measureFreq)
{
    pwm->setMeasureFreq(measureFreq);
}

void MainWindow::setUseFeccTable1()
{
    pwm->setFECCTableNumber(1);
}

void MainWindow::setUseFeccTable2()
{
    int feccTablesCount;
    pwm->getFECCTablesCount(feccTablesCount);

    if (feccTablesCount == 2)
        pwm->setFECCTableNumber(2);
}

void MainWindow::setPowerUnitsTypeSlot(pwmPowerUnits powerUnits)
{
    double power;
    pwm->setPowerUnits(powerUnits);
    pwm->calculatePower(voltageValue, power);
    measurementWidget->setPowerValue(voltageValue, power, powerUnits);
}
