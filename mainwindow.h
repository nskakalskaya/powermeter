#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>
#include <QTimer>
#include <QMessageBox>

#include "Widgets/openpowermeter.h"
#include "Widgets/loggingwidget.h"
#include "Widgets/measurementwidget.h"
#include "Widgets/optionswidget.h"
#include "Widgets/fecc.h"

#include "powermeter.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT


    const int READ_V_INTERVAL = 5;
    const int OUTPUT_INTERVAL = 500;
    PowerMeter *pwm;
    OpenPowerMeter *openPowerMeter;
    MeasurementWidget *measurementWidget;
    OptionsWidget *optionsWidget;
    LoggingWidget *outputLog;
    FECC *fecc;
    QTimer *autoUpdateTimer;
    int lastAverageCount;
    int voltageCounter;
    double voltageSum;
    double voltageValue;

    void initVoltageCounter();
    QString getPWMStatusString(int status);

public:
    MainWindow(QWidget *parent = 0);
    void keyPressEvent(QKeyEvent *event);

    ~MainWindow();

public slots:
    void startAutoTimer();
    void stopAutoTimer();
    void readVoltageFromPowermeter();
    void refreshDeviceList();
    void connectSlot(int sN);
    void disconnectSlot();

    void setAverageCountSlot(int count);
    void setMeasureFreqSlot(double measureFreq);
    void setUseFeccTable1();
    void setUseFeccTable2();

    void setPowerUnitsTypeSlot(pwmPowerUnits powerUnits);


};

#endif // MAINWINDOW_H
