#-------------------------------------------------
#
# Project created by QtCreator 2015-09-08T15:27:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += c++11

TARGET = PowerMeter
TEMPLATE = app

# =========================================================================== #
#   Set version here and rebuild
#   Version = MAJ.MIN.PAT, where:
#       MAJ - Major version
#       MIN - Minor version
#       PAT - Patch number
    VER_MAJ = 1
    VER_MIN = 1
    VER_PAT = 0

#   Optional suffix denoting experimental branch
#    (must begin with two underscores, leave undefined if on main branch)
    VER_TESTNAME =

# =========================================================================== #

# Define version string
DEFINES += VERSION_STRING=\\\"$${VER_MAJ}.$${VER_MIN}.$${VER_PAT}$${VER_TESTNAME}\\\"


RC_ICONS += powerMeter.ico

HEADERS += \
    mainwindow.h \
    Widgets/measurementwidget.h \
    Widgets/optionswidget.h \
    Widgets/loggingwidget.h \
    Widgets/fecc.h \
    Widgets/openpowermeter.h

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    Widgets/measurementwidget.cpp \
    Widgets/optionswidget.cpp \
    Widgets/loggingwidget.cpp \
    Widgets/fecc.cpp \
    Widgets/openpowermeter.cpp

RESOURCES += \
    icon.qrc

#-------------------------------------------------------------
# SiLabs USBxpress
#-------------------------------------------------------------
LIBS += -L$$PWD/3rdparty/siusb/lib/ -lSiUSBXp
INCLUDEPATH += $$PWD/3rdparty/siusb/include

#-------------------------------------------------------------
# PowerMeter
#-------------------------------------------------------------
PWM_PATH = $$PWD/../PowerMeterLibrary/powerMeter
INCLUDEPATH += $${PWM_PATH}

HEADERS += $${PWM_PATH}/typesanderrors.h
HEADERS += $${PWM_PATH}/crc32.h
HEADERS += $${PWM_PATH}/dataclass.h
HEADERS += $${PWM_PATH}/powermeter.h
SOURCES += $${PWM_PATH}/crc32.cpp
SOURCES += $${PWM_PATH}/dataclass.cpp
SOURCES += $${PWM_PATH}/powermeter.cpp
