#ifndef MEASUREMENTWIDGET_H
#define MEASUREMENTWIDGET_H

#include <QGroupBox>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QTextEdit>
#include <QLabel>
#include <QPushButton>
#include <QPicture>

#include "dataclass.h"
#include "typesanderrors.h"


class MeasurementWidget : public QGroupBox
{
    Q_OBJECT

    QSpinBox *averageCountSpinBox;
    QDoubleSpinBox *freqSpinBox;
    QComboBox *stepComboBox;
    QLabel *powerValueLabel;
    QLabel *powerUnitsLabel;
    QLabel *overloadLabel;
    QLabel *freqRangeLabel;
    QPushButton *writeButton;
    QPixmap overloadPixmap;
    double maxVoltage;

    void setStep(QString step);
    void readPressed();

public:
    MeasurementWidget(QWidget *parent = 0);
    ~MeasurementWidget();

signals:
    void sendFreq(double freq);
    void measurePress();
    void sendMeasure(QString measure);
    void sendAverageCount(int count);

public slots:
    void setPowerValue(double voltage, double powerValue, pwmPowerUnits powerUnits);
    void setBasicData(BasicDataStruct basicData);
    void onConnected();
    void onDisconnected();
    void passAverageCount();
    void deselectSpinBox();     // Kill spinboxes text highlight
    void setEnableMeasurements(bool enable);

};

#endif // MEASUREMENTWIDGET_H
