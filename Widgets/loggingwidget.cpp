#include "loggingwidget.h"

#include <QPushButton>
#include <QLayout>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>


LoggingWidget::LoggingWidget(QWidget *parent): QGroupBox(parent)
{
    QFont smallFont("Arial", 11);

    measuresTextEdit = new QTextEdit(this);
    measuresTextEdit->setFixedWidth(250);
    measuresTextEdit->setFont(smallFont);
    measuresTextEdit->setReadOnly(true);
    clearMeasuresButton = new QPushButton(QIcon(":/icons/edit_clear"), "Clear", this);
    clearMeasuresButton->setFixedSize(80, 30);
    clearMeasuresButton->setFont(smallFont);
    clearMeasuresButton->setIconSize(QSize(75, 25));
    saveMeasuresButton = new QPushButton(QIcon(":/icons/save"), "Save", this);
    saveMeasuresButton->setFixedSize(80, 30);
    saveMeasuresButton->setFont(smallFont);
    saveMeasuresButton->setIconSize(QSize(75, 25));
    saveMeasuresButton->setEnabled(false);

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(clearMeasuresButton);
    buttonLayout->addWidget(saveMeasuresButton);

    QVBoxLayout *measuaresLayout = new QVBoxLayout;
    measuaresLayout->addWidget(measuresTextEdit, 0, Qt::AlignHCenter);
    measuaresLayout->addLayout(buttonLayout);

    setLayout(measuaresLayout);

    connect(clearMeasuresButton, &QPushButton::clicked, this, &LoggingWidget::clearMeasures);
    connect(saveMeasuresButton, &QPushButton::clicked, this, &LoggingWidget::saveMeasures);
}

void LoggingWidget::saveMeasures()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save data"), QDir::currentPath(), tr("Data Files (*.txt)"));

    if (fileName == "")
        return;

    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::warning(this, "Error", "Failed to save file");
        return;
    }

    QTextStream out(&file);
    out << measuresTextEdit->toPlainText();
    file.close();
}

void LoggingWidget::clearMeasures()
{
    measuresTextEdit->clear();
    saveMeasuresButton->setEnabled(false);
}

void LoggingWidget::addMeasure(QString line)
{
    measuresTextEdit->append(line);
    saveMeasuresButton->setEnabled(true);
}

LoggingWidget::~LoggingWidget()
{
}

