#include "optionswidget.h"

#include <QRadioButton>
#include <QLayout>


OptionsWidget::OptionsWidget(QWidget *parent): QGroupBox(parent)
{
    QFont smallFont("Arial", 11);

    QRadioButton *autoRadioButton = new QRadioButton("Auto", this);
    autoRadioButton->setFont(smallFont);
    autoRadioButton->setChecked(true);
    QRadioButton *mWRadioButton = new QRadioButton("mW", this);
    mWRadioButton->setFont(smallFont);
    QRadioButton *uWRadioButton = new QRadioButton("uW", this);
    uWRadioButton->setFont(smallFont);
    QRadioButton *dDmRadioButton = new QRadioButton("dBm", this);
    dDmRadioButton->setFont(smallFont);


    QHBoxLayout *powerUnitsLayout = new QHBoxLayout;
    powerUnitsLayout->addStretch(1);
    powerUnitsLayout->addWidget(autoRadioButton);
    powerUnitsLayout->addStretch(1);
    powerUnitsLayout->addWidget(mWRadioButton);
    powerUnitsLayout->addStretch(1);
    powerUnitsLayout->addWidget(uWRadioButton);
    powerUnitsLayout->addStretch(1);
    powerUnitsLayout->addWidget(dDmRadioButton);
    powerUnitsLayout->addStretch(1);

    setLayout(powerUnitsLayout);

    connect(autoRadioButton, &QRadioButton::clicked, this, &OptionsWidget::sendAutoEnabled);
    connect(mWRadioButton, &QRadioButton::clicked, this, &OptionsWidget::sendMWEnabled);
    connect(uWRadioButton, &QRadioButton::clicked, this, &OptionsWidget::sendUWEnabled);
    connect(dDmRadioButton, &QRadioButton::clicked, this, &OptionsWidget::sendDBMEnabled);
}

void OptionsWidget::sendAutoEnabled()
{
    emit sendPowerUnits(PWM_POWER_AUTO);
}

void OptionsWidget::sendMWEnabled()
{
    emit sendPowerUnits(PWM_POWER_MW);
}

void OptionsWidget::sendUWEnabled()
{
    emit sendPowerUnits(PWM_POWER_UW);
}

void OptionsWidget::sendDBMEnabled()
{
    emit sendPowerUnits(PWM_POWER_DBM);
}

OptionsWidget::~OptionsWidget()
{

}
