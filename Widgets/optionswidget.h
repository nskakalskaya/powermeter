#ifndef OPTIONSWIDGET_H
#define OPTIONSWIDGET_H

#include <QGroupBox>

#include "dataclass.h"
#include "typesanderrors.h"


class OptionsWidget : public QGroupBox
{
    Q_OBJECT

public:
    OptionsWidget(QWidget *parent = 0);
    ~OptionsWidget();

signals:
    void sendPowerUnits(pwmPowerUnits powerUnits);

public slots:
    void sendAutoEnabled();
    void sendMWEnabled();
    void sendUWEnabled();
    void sendDBMEnabled();
};

#endif // OPTIONSWIDGET_H
