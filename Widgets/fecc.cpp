#include "fecc.h"

#include <QLayout>
#include <QDebug>


FECC::FECC(QWidget *parent): QGroupBox(parent)
{
    QFont smallFont("Arial", 11);

    useFeccTab1RadioButton = new QRadioButton("Correction 1", this);
    useFeccTab1RadioButton->setFixedWidth(115);
    useFeccTab1RadioButton->setFont(smallFont);
    useFeccTab1RadioButton->setChecked(true);
    useFeccTab2RadioButton = new QRadioButton("Correction 2", this);
    useFeccTab2RadioButton->setFixedWidth(115);
    useFeccTab2RadioButton->setFont(smallFont);

    QHBoxLayout *radioButtonLayout = new QHBoxLayout;
    radioButtonLayout->addWidget(useFeccTab1RadioButton, 1);
    radioButtonLayout->addWidget(useFeccTab2RadioButton, 1);

    setLayout(radioButtonLayout);

    connect(useFeccTab1RadioButton, &QRadioButton::clicked, this, &FECC::sendUseFeccTable1);
    connect(useFeccTab2RadioButton, &QRadioButton::clicked, this, &FECC::sendUseFeccTable2);

    setCountOfFeccTable(1);
}

void FECC::setCountOfFeccTable(int countOfFeccTable)
{
    if (countOfFeccTable == 1)
    {
        useFeccTab1RadioButton->setChecked(true);
        useFeccTab2RadioButton->hide();
        this->hide();
    }
    else if (countOfFeccTable == 2)
    {
        useFeccTab2RadioButton->show();
        this->show();
    }
}

FECC::~FECC()
{

}

