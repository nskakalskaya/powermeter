#include "measurementwidget.h"

#include <qmath.h>

#include <QLayout>
#include <QLineEdit>
#include <QDebug>


MeasurementWidget::MeasurementWidget(QWidget *parent) :
    QGroupBox(parent),
    maxVoltage(1.0)
{
    QFont smallFont("Arial", 11);
    QFont mediumFont("Arial", 15);
    QFont bigFont("Arial", 35);
    overloadPixmap = QPixmap(":/icons/danger");

    freqRangeLabel = new QLabel(this);
    freqRangeLabel->setFont(mediumFont);

    stepComboBox = new QComboBox(this);
    stepComboBox->setFixedSize(70, 30);
    stepComboBox->setFont(mediumFont);
    stepComboBox->addItem("1.00");
    stepComboBox->addItem("0.50");
    stepComboBox->addItem("0.25");
    stepComboBox->addItem("0.20");
    stepComboBox->addItem("0.10");
    stepComboBox->addItem("0.05");
    stepComboBox->addItem("0.02");
    stepComboBox->addItem("0.01");
    stepComboBox->setEditable(true);
    stepComboBox->lineEdit()->setReadOnly(true);

    freqSpinBox = new QDoubleSpinBox(this);
    freqSpinBox->setFixedSize(190, 45);
    freqSpinBox->setFont(bigFont);
    freqSpinBox->setSingleStep(1);

    overloadLabel = new QLabel(this);
    overloadLabel->setFixedSize(80, 80);

    powerValueLabel = new QLabel(this);
    powerValueLabel->setFixedSize(190, 45);
    powerValueLabel->setFont(bigFont);
    powerValueLabel->setStyleSheet("background-color : white;  border-radius: 3px; border: 1px solid black;");

    powerUnitsLabel = new QLabel(this);
    powerUnitsLabel->setFixedSize(40, 30);
    powerUnitsLabel->setFont(mediumFont);
    powerUnitsLabel->setStyleSheet("qproperty-alignment: AlignCenter;");

    writeButton = new QPushButton(QIcon(":/icons/edit"), "Write", this);
    writeButton->setFixedSize(80, 30);
    writeButton->setFont(smallFont);
    writeButton->setIconSize(QSize(75, 25));

    averageCountSpinBox = new QSpinBox(this);
    averageCountSpinBox->setFixedSize(70, 30);
    averageCountSpinBox->setFont(mediumFont);
    averageCountSpinBox->setMinimum(1);
    averageCountSpinBox->setMaximum(1000);
    averageCountSpinBox->setSingleStep(1);
    averageCountSpinBox->clear();

    QLabel *rangeLabel = new QLabel("Range:", this);
    rangeLabel->setFont(mediumFont);

    QLabel *rangeGHzLabel = new QLabel("GHz", this);
    rangeGHzLabel->setFixedSize(40, 30);
    rangeGHzLabel->setFont(mediumFont);

    QLabel *frequencyLabel = new QLabel("Frequency:", this);
    frequencyLabel->setFont(mediumFont);

    QLabel *frequencyGHzLabel = new QLabel("GHz", this);
    frequencyGHzLabel->setFixedSize(40, 30);
    frequencyGHzLabel->setFont(mediumFont);

    QLabel *stepLabel = new QLabel("Step:", this);
    stepLabel->setFont(mediumFont);

    QLabel *stepGHzLabel = new QLabel("GHz", this);
    stepGHzLabel->setFixedSize(40, 30);
    stepGHzLabel->setFont(mediumFont);

    QLabel *AverageCountLabel = new QLabel("Average Count:", this);
    AverageCountLabel->setFont(mediumFont);

    QHBoxLayout *freqRangeLayout = new QHBoxLayout;
    freqRangeLayout->addSpacing(20);
    freqRangeLayout->addWidget(rangeLabel);
    freqRangeLayout->addStretch(1);
    freqRangeLayout->addWidget(freqRangeLabel);
    freqRangeLayout->addWidget(rangeGHzLabel);
    freqRangeLayout->addSpacing(20);

    QHBoxLayout *frequencyLayout = new QHBoxLayout;
    frequencyLayout->addSpacing(20);
    frequencyLayout->addWidget(frequencyLabel, 0, Qt::AlignHCenter);
    frequencyLayout->addStretch(1);
    frequencyLayout->addWidget(freqSpinBox, 0, Qt::AlignHCenter);
    frequencyLayout->addWidget(frequencyGHzLabel, 0, Qt::AlignHCenter);
    frequencyLayout->addSpacing(20);

    QHBoxLayout *stepLayout = new QHBoxLayout;
    stepLayout->addSpacing(20);
    stepLayout->addWidget(stepLabel, 0, Qt::AlignHCenter);
    stepLayout->addStretch(1);
    stepLayout->addWidget(stepComboBox, 0, Qt::AlignHCenter);
    stepLayout->addWidget(stepGHzLabel, 0, Qt::AlignHCenter);
    stepLayout->addSpacing(20);

    QHBoxLayout *outputLayout = new QHBoxLayout;
    outputLayout->addSpacing(20);
    outputLayout->addWidget(overloadLabel);
    outputLayout->addStretch(1);
    outputLayout->addWidget(powerValueLabel);
    outputLayout->addWidget(powerUnitsLabel);
    outputLayout->addSpacing(20);

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addSpacing(20);
    buttonLayout->addWidget(writeButton);
    buttonLayout->addStretch(1);
    buttonLayout->addWidget(AverageCountLabel);
    buttonLayout->addWidget(averageCountSpinBox);
    buttonLayout->addSpacing(20);

    QVBoxLayout *measLayout = new QVBoxLayout;
    measLayout->addLayout(stepLayout);
    measLayout->addSpacing(20);
    measLayout->addLayout(frequencyLayout);
    measLayout->addSpacing(20);
    measLayout->addLayout(outputLayout);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addSpacing(10);
    mainLayout->addLayout(freqRangeLayout);
    mainLayout->addSpacing(20);
    mainLayout->addLayout(measLayout);
    mainLayout->addSpacing(30);
    mainLayout->addLayout(buttonLayout);
    mainLayout->addSpacing(10);

    setLayout(mainLayout);

    connect(stepComboBox, &QComboBox::currentTextChanged, this, &MeasurementWidget::setStep);
    connect(averageCountSpinBox, static_cast <void(QSpinBox::*)(int)> (&QSpinBox::valueChanged), this, &MeasurementWidget::sendAverageCount);
    connect(averageCountSpinBox, static_cast <void(QSpinBox::*)(int)> (&QSpinBox::valueChanged), this, &MeasurementWidget::deselectSpinBox, Qt::QueuedConnection);
    connect(freqSpinBox, static_cast <void(QDoubleSpinBox::*)(double)> (&QDoubleSpinBox::valueChanged), this, &MeasurementWidget::deselectSpinBox, Qt::QueuedConnection);
    connect(writeButton, &QPushButton::clicked, this, &MeasurementWidget::readPressed);
    connect(freqSpinBox, SIGNAL(valueChanged(double)), this, SIGNAL(sendFreq(double)));
}

MeasurementWidget::~MeasurementWidget()
{
}

void MeasurementWidget::onConnected()
{
    setEnableMeasurements(true);
}

void MeasurementWidget::onDisconnected()
{
    setEnableMeasurements(false);
    freqRangeLabel->setText("");
    powerValueLabel->setText("");
    powerUnitsLabel->setText("");
    overloadLabel->clear();
}

void MeasurementWidget::passAverageCount()
{
    if (averageCountSpinBox->value() > 0)
        emit sendAverageCount(averageCountSpinBox->value());
}

void MeasurementWidget::deselectSpinBox()
{
    freqSpinBox->findChild<QLineEdit*>()->deselect();
    averageCountSpinBox->findChild<QLineEdit*>()->deselect();
}

void MeasurementWidget::setEnableMeasurements(bool enable)
{
    stepComboBox->setEnabled(enable);
    freqSpinBox->setEnabled(enable);
    averageCountSpinBox->setEnabled(enable);
    writeButton->setEnabled(enable);

    if (enable)
    {
        powerValueLabel->show();
        powerUnitsLabel->show();
    }
    else
    {
        powerValueLabel->hide();
        powerUnitsLabel->hide();
        freqSpinBox->clear();
        averageCountSpinBox->clear();
    }
}

void MeasurementWidget::readPressed()
{
    emit sendMeasure(freqSpinBox->text() + "   GHz            " + powerValueLabel->text() + "   " + powerUnitsLabel->text());
}

void MeasurementWidget::setBasicData(BasicDataStruct basicData)
{
    freqRangeLabel->setText(QString::number(basicData.fmin, 'f', 2) + " — " + QString::number(basicData.fmax, 'f', 2));
    freqSpinBox->setMinimum(basicData.fmin);
    freqSpinBox->setMaximum(basicData.fmax);
    freqSpinBox->setValue(basicData.fmin);
    emit sendFreq(basicData.fmin);

    averageCountSpinBox->setValue(basicData.defaultAverageCount);
    maxVoltage = basicData.overloadV;
}

void MeasurementWidget::setStep(QString step)
{
    double st = 0;
    bool ok = false;
    st = step.toDouble(&ok);
    if (ok)
        freqSpinBox->setSingleStep(st);
}

void MeasurementWidget::setPowerValue(double voltage, double powerValue, pwmPowerUnits powerUnits)
{
    QColor color;
    QPalette palette;

// Check for overloading
    if (voltage < 0.9 * maxVoltage)
    {
        color = qRgb(0, 0, 0);
        palette.setColor(QPalette::WindowText, color);
        powerValueLabel->setPalette(palette);
        overloadLabel->clear();
    }
    else
    {
        color = qRgb(255, 0, 0);
        palette.setColor(QPalette::WindowText, color);
        powerValueLabel->setPalette(palette);

        if (voltage >= maxVoltage)
            overloadLabel->setPixmap(overloadPixmap.scaled(80, 80, Qt::KeepAspectRatio, Qt::SmoothTransformation));
        else
            overloadLabel->clear();
    }

// Output power
    int powerPrec = 2;
    if (powerValue < 10.0)
       powerPrec = 3;

    if (powerUnits == PWM_POWER_MW || powerUnits == PWM_POWER_AUTO_MW)
    {
        powerUnitsLabel->setText("mW");
    }
    else if (powerUnits == PWM_POWER_UW || powerUnits == PWM_POWER_AUTO_UW)
    {
        powerUnitsLabel->setText("uW");
    }
    else if (powerUnits == PWM_POWER_DBM)
    {
        powerPrec = 1;
        powerUnitsLabel->setText("dBm");
    }

    powerValueLabel->setText(QString::number(powerValue, 'f', powerPrec));
}
