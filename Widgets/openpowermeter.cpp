#include "openpowermeter.h"

#include <QLayout>
#include <QDebug>


OpenPowerMeter::OpenPowerMeter(QWidget *parent): QGroupBox(parent)
{
    QFont smallFont("Arial", 11);
    QFont mediumFont("Arial", 15);
    connectColor.setNamedColor("#00ff00");
    disconnectColor.setNamedColor("#ff0000");

    deviceComboBox = new QComboBox(this);
    deviceComboBox->setFont(mediumFont);
    deviceComboBox->setFixedWidth(70);
    deviceComboBox->setFixedHeight(30);

    connectButton = new QPushButton(QIcon(":/icons/disconnect"), "Connect", this);
    connectButton->setFont(smallFont);
    connectButton->setFixedSize(110, 30);
    connectButton->setIconSize(QSize(75, 25));

    refreshButton = new QPushButton(QIcon(":/icons/refresh"), "Refresh", this);
    refreshButton->setFont(smallFont);
    refreshButton->setFixedSize(110, 30);
    refreshButton->setIconSize(QSize(75, 25));

    connectLabel = new QLabel(this);
    connectLabel->setPalette(QPalette(disconnectColor));
    connectLabel->setAutoFillBackground(true);
    connectLabel->setFixedSize(70, 30);
    connectLabel->setAlignment(Qt::AlignCenter);

    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(deviceComboBox);
    mainLayout->addWidget(refreshButton);
    mainLayout->addWidget(connectButton);
    mainLayout->addWidget(connectLabel);

    setLayout(mainLayout);

    connect(connectButton, &QPushButton::pressed, this, &OpenPowerMeter::connectToPowerMeterSlot);
    connect(refreshButton, &QPushButton::pressed, this, &OpenPowerMeter::refreshList);
}

void OpenPowerMeter::onConnected()
{
    deviceComboBox->setEnabled(false);
    refreshButton->setEnabled(false);
    connect(connectButton, &QPushButton::pressed, this, &OpenPowerMeter::disconnectToPowerMeterSignal);
    disconnect(connectButton, &QPushButton::pressed, this, &OpenPowerMeter::connectToPowerMeterSlot);
    connectButton->setIcon(QIcon(":/icons/connect"));
    connectButton->setText("Disconnect");
    connectLabel->setPalette(QPalette(connectColor));
}

void OpenPowerMeter::onDisconnected()
{
    deviceComboBox->setEnabled(true);
    refreshButton->setEnabled(true);
    disconnect(connectButton, &QPushButton::pressed, this, &OpenPowerMeter::disconnectToPowerMeterSignal);
    connect(connectButton, &QPushButton::pressed, this, &OpenPowerMeter::connectToPowerMeterSlot);
    connectButton->setIcon(QIcon(":/icons/disconnect"));
    connectButton->setText("Connect");
    connectLabel->setPalette(QPalette(disconnectColor));
    connectLabel->setText("");
}

void OpenPowerMeter::setDeviceList(std::vector<int> deviceList)
{
    deviceComboBox->clear();

    if (!deviceList.size())
    {
        connectButton->setEnabled(false);
    }
    else
    {
        connectButton->setEnabled(true);
        for (int i = 0; i < deviceList.size(); i++)
            deviceComboBox->addItem(QString::number(deviceList[i]));
    }
}

void OpenPowerMeter::connectToPowerMeterSlot()
{
    emit connectToPowerMeterSignal(deviceComboBox->currentText().toInt());
}

OpenPowerMeter::~OpenPowerMeter()
{

}
