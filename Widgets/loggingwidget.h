#ifndef LOGGINGWIDGET_H
#define LOGGINGWIDGET_H

#include <QGroupBox>
#include <QTextEdit>
#include <QMessageBox>


class LoggingWidget : public QGroupBox
{
    Q_OBJECT

    QPushButton *clearMeasuresButton;
    QPushButton *saveMeasuresButton;
    QTextEdit *measuresTextEdit;

    void saveMeasures();
    void clearMeasures();

public:
    LoggingWidget(QWidget *parent = 0);
    ~LoggingWidget();

public slots:
    void addMeasure(QString line);
};

#endif // LOGGINGWIDGET_H
