#ifndef FECC_H
#define FECC_H

#include <QGroupBox>
#include <QRadioButton>

class FECC : public QGroupBox
{
    Q_OBJECT
    QRadioButton *useFeccTab1RadioButton, *useFeccTab2RadioButton;


public:
    FECC(QWidget *parent = 0);
    ~FECC();

signals:
    void sendUseFeccTable1();
    void sendUseFeccTable2();

public slots:
    void setCountOfFeccTable(int countOfFeccTable);//установить число таблиц с поправочными коэфициентами, может

};

#endif // FECC_H
