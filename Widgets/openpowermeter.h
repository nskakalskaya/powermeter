#ifndef OPENPOWERMETER_H
#define OPENPOWERMETER_H

#include <QGroupBox>
#include <QComboBox>
#include <QPushButton>
#include <QLabel>
#include <QColor>
#include <QStringList>


class OpenPowerMeter : public QGroupBox
{
    Q_OBJECT

    QComboBox *deviceComboBox;
    QPushButton *connectButton;
    QPushButton *refreshButton;
    QLabel *connectLabel;
    QColor connectColor;
    QColor disconnectColor;

public:
    OpenPowerMeter(QWidget *parent = 0);
    ~OpenPowerMeter();

signals:
    void connectToPowerMeterSignal(int sN);
    void disconnectToPowerMeterSignal();
    void refreshList();

public slots:
    void connectToPowerMeterSlot();
    void onConnected();
    void onDisconnected();
    void setDeviceList(std::vector<int> deviceList);

};

#endif // OPENPOWERMETER_H
