#include <QApplication>
#include <QDesktopWidget>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QRect rect = QApplication::desktop()->availableGeometry();
    int wWidth = 700;
    int wHeight = 550;

    MainWindow w;
    w.setWindowTitle("PowerMeter " + QString(VERSION_STRING));
    w.setFixedSize(wWidth, wHeight);
    w.move((rect.width() - wWidth) / 2, (rect.height() - wHeight) / 2);
    w.show();

    return a.exec();
}
